
import cv2
from scene_manager_cuts import custom_manager

class VideoProcessing():

    def inicializeVideo(self):

        self.video = cv2.VideoCapture(self.video_path)
        #self.ret, self.frame = self.video.read()

    def configureVideo(self):

        self.ret = self.video.set(3,1280)
        self.ret = self.video.set(4,720)
        self.frame_width, self.frame_height = int(self.video.get(3)),int(self.video.get(4))
        self.custom = custom_manager()
        self.start_frame, self.end_frame, self.total_frames, self.current_frame = self.custom.start_manager(self.video)
        self.interval_cuts = 2
        self.conf_threshold = .6
        self.video_saver = cv2.VideoWriter((self.video_title+'.avi'),cv2.VideoWriter_fourcc('M','J','P','G'), 25, (self.frame_width, self.frame_height))
        
    
    def detectScenes(self, frame):
        
        self.frame = frame
        if self.current_frame%self.interval_cuts==0:
            self.cuts = self.custom.process_frame(self.current_frame + self.start_frame, self.frame)   
        else: 
            self.cuts = []
        self.custom.cuts_list+=self.cuts

        self.current_frame += 1
        print("Frame number: ",self.current_frame)

    def SaveScenes(self, frame):        
        # Save scene
        if self.save_video_mode=='cut':
            if self.cuts:
                self.scene_number += 1
                self.video_saver = cv2.VideoWriter((self.video_title+'_'+str(self.scene_number)+'.avi'),cv2.VideoWriter_fourcc('M','J','P','G'), 25, (self.frame_width, self.frame_height))
            self.video_saver.write(self.frame)
        
        # Save full video
        elif self.save_video_mode=='full':
            self.video_saver.write(self.frame) 
    



    def convertVideoToImages(self, video_path, save_video_path, video_name):

        self.video_path = video_path
        #self.inicializeVideo()
        self.save_video_path = save_video_path
        self.video_name = video_name
        count = 0
        while True:
            ret, frame = self.video.read()
            if count%10 == 0 and ret:
                print("HERE!", count)
                cv2.imwrite(self.save_video_path + '/' + self.video_name + '.jpg', frame) #% (count)# save frame as JPEG file
                print('Read a new frame: ', ret, count)
            count += 1

    
    def __init__(self, video_path, video_title, save_video_mode):

        self.video_path = video_path
        self.video_title = video_title
        self.save_video_mode = save_video_mode
        self.video_saver = None
        self.scene_number = 0

        self.inicializeVideo()
        self.configureVideo()
        #self.cutAndSaveScenes(self.frame)
        

