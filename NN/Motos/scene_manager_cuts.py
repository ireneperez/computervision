
#
# PySceneDetect v0.5 API Test Script
#
# NOTE: This file can only be used with development versions of PySceneDetect,
#       and gives a high-level overview of how the new API will look and work.
#       This file is for development and testing purposes mostly, although it
#       also serves as a base for further example and test programs.
#

from __future__ import print_function
import os

import scenedetect
import math
import cv2
import numpy as np
from scenedetect.frame_timecode import FrameTimecode



class custom_manager():
    def __init__(self):
        self.last_frame = None
        self.last_hsv = None
        self.last_scene_cut = None
        self.min_scene_len = 15
        self.stats_manager = None
        self.threshold = 30.0
        self._num_frames = 0
        self._start_frames = 0
        self._metric_keys = ['content_val', 'delta_hue', 'delta_sat', 'delta_lum']
        self.cuts_list = []

    def process_frame(self,frame_num, frame_img):
            # type: (int, np.ndarray) -> List[int]
            """ Similar to ThresholdDetector, but using the HSV colour space DIFFERENCE instead
            of single-frame RGB/grayscale intensity (thus cannot detect slow fades with this method)."""

            cut_list = []
            metric_keys = self._metric_keys
            _unused = ''

            if self.last_frame is not None:
                # Change in average of HSV (hsv), (h)ue only, (s)aturation only, (l)uminance only.
                delta_hsv_avg, delta_h, delta_s, delta_v = 0.0, 0.0, 0.0, 0.0

                if (self.stats_manager is not None and
                        self.stats_manager.metrics_exist(frame_num, metric_keys)):
                    delta_hsv_avg, delta_h, delta_s, delta_v = self.stats_manager.get_metrics(
                        frame_num, metric_keys)

                else:
                    num_pixels = frame_img.shape[0] * frame_img.shape[1]
                    curr_hsv = cv2.split(cv2.cvtColor(frame_img, cv2.COLOR_BGR2HSV))
                    last_hsv = self.last_hsv
                    if not last_hsv:
                        last_hsv = cv2.split(cv2.cvtColor(self.last_frame, cv2.COLOR_BGR2HSV))

                    delta_hsv = [0, 0, 0, 0]
                    for i in range(3):
                        num_pixels = curr_hsv[i].shape[0] * curr_hsv[i].shape[1]
                        curr_hsv[i] = curr_hsv[i].astype(np.int32)
                        last_hsv[i] = last_hsv[i].astype(np.int32)
                        delta_hsv[i] = np.sum(
                            np.abs(curr_hsv[i] - last_hsv[i])) / float(num_pixels)
                    delta_hsv[3] = sum(delta_hsv[0:3]) / 3.0
                    delta_h, delta_s, delta_v, delta_hsv_avg = delta_hsv

                    if self.stats_manager is not None:
                        self.stats_manager.set_metrics(frame_num, {
                            metric_keys[0]: delta_hsv_avg,
                            metric_keys[1]: delta_h,
                            metric_keys[2]: delta_s,
                            metric_keys[3]: delta_v})

                    self.last_hsv = curr_hsv

                if delta_hsv_avg >= self.threshold:
                    if self.last_scene_cut is None or (
                            (frame_num - self.last_scene_cut) >= self.min_scene_len):
                        cut_list.append(frame_num)
                        self.last_scene_cut = frame_num

                if self.last_frame is not None and self.last_frame is not _unused:
                    del self.last_frame

            # If we have the next frame computed, don't copy the current frame
            # into last_frame since we won't use it on the next call anyways.
            if (self.stats_manager is not None and
                    self.stats_manager.metrics_exist(frame_num+1, metric_keys)):
                self.last_frame = _unused
            else:
                self.last_frame = frame_img.copy()
            return cut_list

    def start_manager(self,frame_source,end_time=None):
        start_frame = 0
        curr_frame = 0
        end_frame = None
        total_frames = math.trunc(frame_source.get(cv2.CAP_PROP_FRAME_COUNT))
        start_time = frame_source.get(cv2.CAP_PROP_POS_FRAMES)
        if isinstance(start_time, FrameTimecode):
            start_frame = start_time.get_frames()
        elif start_time is not None:
            start_frame = int(start_time)
        self._start_frame = start_frame
        curr_frame = start_frame
        if isinstance(end_time, FrameTimecode):
            end_frame = end_time.get_frames()
        elif end_time is not None:
            end_frame = int(end_time)
        if end_frame is not None:
            total_frames = end_frame
        if start_frame is not None and not isinstance(start_time, FrameTimecode):
            total_frames -= start_frame
        if total_frames < 0:
            total_frames = 0        
        return start_frame,end_frame,total_frames,curr_frame

def test_api():
    print("Running PySceneDetect API test...")
    cap = cv2.VideoCapture('videos/qatar2019_short.mp4')
    custom = custom_manager()
    # num_frames,cut_list = custom.detect_scenes(frame_source=cap)
    start_frame,end_frame,total_frames,curr_frame = custom.start_manager(cap)
    while True:
        ret_val, frame_im = cap.read()
        if not ret_val:
            break
        cuts = custom.process_frame(custom._num_frames + start_frame, frame_im)
        custom.cuts_list += cuts
        curr_frame += 1
        custom._num_frames += 1
        if cuts:
            print(cuts[0])
        num_frames = curr_frame - start_frame
        
    cut_list = sorted(list(set(custom.cuts_list)))

    print('List of cuts obtained:',num_frames,cut_list)


if __name__ == "__main__":
    test_api()

