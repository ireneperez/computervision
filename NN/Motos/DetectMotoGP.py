import sys
sys.path.append("..")


# Import packages
import os
import cv2
import numpy as np
import sys
import time
from tracking_algorithm import main_track
from utils import label_map_util
from utils import visualization_utils as vis_util
import argparse
from computing_classes import main_class
from TensorClass import Tensor
from VideoProcessingClass import VideoProcessing

def launch():

    # Arguments to pass
    parser = argparse.ArgumentParser()
    parser.add_argument('--video_input',default='./videos/argentina_2019highlights.mp4',help='i')
    parser.add_argument('--video_title',default='result',help='Enter video title')
    parser.add_argument('--display',default='Yes',help='d')
    parser.add_argument('--save',default='full',help='full,cut, or nothing')
    args = parser.parse_args()
    video_path = args.video_input
    video_title = args.video_title
    disp = args.display
    save_mode = args.save

    #Inicilize tensors
    tensor_escuderias = Tensor('inference_graph_escuderias','training_escuderias',2)
    print("Tensor Escuderías Inicializado")

    tensor_marcas = Tensor('inference_graph_marcas','training_marcas',7)
    print("Tensor Marcas Inicializado")

    #Inicialize video
    video_processing = VideoProcessing(video_path, video_title, save_mode)

    # print("Procesando vídeo...")
    # print(video_path)
    # video_processing.convertVideoToImages(video_path, save_video_path = video_output, video_name = video_title)
    # print("Video procesado")

    while True:
        # Read frames
        ret, frame = video_processing.video.read()
        if ret: 
            frame_expanded = np.expand_dims(frame, axis=0)

            # Perform the actual detection by running the model with the image as input
            tensor_escuderias.performDetection(frame_expanded)

            # crop the image and apply detection from colors and brands
            classes = main_class(frame, frame_expanded, tensor_escuderias, tensor_marcas, video_processing)

            # Detect scenes 
            video_processing.detectScenes(frame)
            
            # Perform tracking of motorbikes
            main_track(tensor_escuderias.boxes_detected,tensor_escuderias.scores_detected,classes,frame,frame_expanded,
                                    video_processing.conf_threshold,tensor_escuderias.categories,video_processing.cuts)

            # Save video
            video_processing.SaveScenes(frame)

            # Display results on the frame
            if disp=='Yes':
                cv2.imshow('Object detector', frame)

            # Press 'q' to quit
            if cv2.waitKey(1) == ord('q'):
                break
        else:
            break
    # Clean up
    video_processing.video.release()
    video_processing.video_saver.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    launch()


