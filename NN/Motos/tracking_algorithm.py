from collections import deque
from sklearn.utils.linear_assignment_ import linear_assignment
import helpers
from helpers import box_iou2
import detector
import tracker
from tracker import Tracker
import numpy as np
import cv2

frame_count = 0 # frame counter
max_age = 5  # no.of consecutive unmatched detection before a track is deleted
min_hits=4  # NO HACE NADA!!!!  # no. of consecutive matches needed to establish a track
same_class = 1 #no. of same motos in a picture
tracker_list =[] # list for trackers
# list for track ID
# track_id_list= deque(['Yamaha','Yamaha','Honda','Honda','Ducati','Ducati'])#'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K'])
track_id_list = []

def assign_detections_to_trackers(trackers, detections, iou_thrd = 0.3):
    IOU_mat = np.zeros((len(trackers),len(detections)),dtype=np.float32)
    for t,trk in enumerate(trackers):
        for d,det in enumerate(detections):
            IOU_mat[t,d]=box_iou2(trk,det)
    matched_idx = linear_assignment(-IOU_mat)

    unmatched_detections,unmatched_trackers = [],[]
    for t,trk in enumerate(trackers):
        if(t not in matched_idx[:,0]):
            unmatched_trackers.append(t)
    for d,det in enumerate(detections):
        if(d not in matched_idx[:,1]):
            unmatched_detections.append(d)
 
# Any with overlap less than iou_thrd is not considered
    matches = []
    for m in matched_idx:
        if(IOU_mat[m[0],m[1]]<iou_thrd):
            unmatched_trackers.append(m[0])
            unmatched_detections.append(m[1])
        else:
            matches.append(m.reshape(1,2))
    if(len(matches)==0):
        matches = np.empty((0,2),dtype=int)
    else:
        matches = np.concatenate(matches,axis=0)
    
    return matches,np.array(unmatched_detections),np.array(unmatched_trackers)

def delete_id(tracker_list,team):
    sep_list,idx_list = [],[]
    for i,trks in enumerate(tracker_list):
        if trks.id==team:
            sep_list.append(trks)
            idx_list.append(i)
    if len(sep_list)>same_class:
        minim_hit = 10000
        for j,tr in enumerate(sep_list):
            if tr.hits < minim_hit:
                minim_hit = tr.hits
                idx_min = j
        tracker_list.remove(tracker_list[idx_list[idx_min]])
    return tracker_list


def pipeline(img,z_box,z_box_class,z_box_scores,cuts):
    global frame_count
    global max_age
    global min_hits
    global tracker_list
    global track_id_list
    global same_class

    frame_count+=1
    # if there's a scene cut, all trackers must be deleted
    if cuts:
        tracker_list = []
    # x_box will have the trackers from previous frame
    x_box = []
    if len(tracker_list)>0:
        for trk in tracker_list:
            x_box.append(trk.box)

    matched, unmatched_dets, unmatched_trks \
    = assign_detections_to_trackers(x_box, z_box, iou_thrd = 0.3)     

    # Deal with matched detections     
    if matched.size >0:
        for trk_idx, det_idx in matched:
            z = z_box[det_idx]
            z = np.expand_dims(z, axis=0).T
            tmp_trk= tracker_list[trk_idx]
            tmp_trk.kalman_filter(z)
            xx = tmp_trk.x_state.T[0].tolist()
            xx =[xx[0], xx[2], xx[4], xx[6]]
            x_box[trk_idx] = xx
            tmp_trk.score = z_box_scores[det_idx]
            tmp_trk.box =xx
            tmp_trk.hits += 1
            if z_box_class[det_idx]==tmp_trk.first_class:
                tmp_trk.class_hits += 1
            tmp_trk.no_losses = 0

    # Deal with unmatched detections
    if len(unmatched_dets)>0:
        for idx in unmatched_dets:
            z = z_box[idx]
            z = np.expand_dims(z, axis=0).T
            tmp_trk = Tracker()
            tmp_trk.x_state = np.array([[z[0], 0, z[1], 0, z[2], 0, z[3], 0]]).T
            #for unmatched_dets we apply only prediction
            tmp_trk.predict_only() 
            # it changes the x_state
            xx = tmp_trk.x_state
            xx = xx.T[0].tolist()
            xx = [xx[0], xx[2], xx[4], xx[6]]
            tmp_trk.box = xx
            tmp_trk.id = z_box_class[idx]
            tmp_trk.first_class = tmp_trk.id
            tmp_trk.score = z_box_scores[idx]
            track_id_list.append(tmp_trk.id)
            tracker_list.append(tmp_trk)
            x_box.append(xx) # no pot ser un array!!!


    # Deal with unmatched tracks       
    if len(unmatched_trks)>0:
        for trk_idx in unmatched_trks:
            tmp_trk = tracker_list[trk_idx]
            tmp_trk.no_losses += 1
            tmp_trk.predict_only()
            xx = tmp_trk.x_state
            xx = xx.T[0].tolist()
            xx =[xx[0], xx[2], xx[4], xx[6]]
            tmp_trk.box =xx
            tmp_trk.score = 0
            x_box[trk_idx] = xx

    # Filter if there are more than 'same_class' detections
    tracker_list = delete_id(tracker_list,'Yamaha')
    tracker_list = delete_id(tracker_list,'Honda')
    tracker_list = delete_id(tracker_list,'Avintia')
    tracker_list = delete_id(tracker_list,'Aprilia')
    tracker_list = delete_id(tracker_list,'Ducati')

    for trk in tracker_list:
        if trk.no_losses <=max_age: # and (trk.hits>=min_hits)):
             x_cv2 = trk.box
             name = trk.id
             score = trk.score
             img= helpers.draw_box_label(img, x_cv2,name,score) # Draw the bounding boxes on the images
    # Book keeping
    deleted_tracks = filter(lambda x: x.no_losses >max_age, tracker_list)  
    for trk in deleted_tracks:
            track_id_list.remove(trk.id)
    tracker_list = [x for x in tracker_list if x.no_losses<=max_age]

    return 0


def main_track(boxes,scores,classes,frame,frame_expanded,conf_threshold,categories,cuts):

    new_boxes = np.squeeze(boxes)
    new_scores = np.squeeze(scores)
    new_classes = np.squeeze(classes)
    z_box,z_box_class,z_box_scores = [],[],[]
    im_width = frame_expanded.shape[2]
    im_height = frame_expanded.shape[1]
    for i in range(new_boxes.shape[0]):
        if new_scores[i] > conf_threshold:
            box = tuple(new_boxes[i].tolist())
            ymin,xmin,ymax,xmax = box
            (left,right,top,bottom) = (xmin*im_width,xmax*im_width,ymin*im_height,ymax*im_height)
            z_box_class.append(classes[i])#categories[int(new_classes[i])-1]['name'] )
            z_box.append([left,top,right,bottom])
            z_box_scores.append(new_scores[i])
    # print("classes detected: ",z_box_class)
    pipeline(frame,z_box,z_box_class,z_box_scores,cuts)