import os, sys
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
# pd.options.mode.chained_assignment = None
import cv2
from skimage import io
import matplotlib
from PIL import Image
import pickle

# LOAD ALL ANNOTATIONS
def load_annotations():
    df = pd.read_csv('/content/drive/My Drive/Colab/M2_vision/materialEnviadoVAMaster/anotaciones_itemsEvaluables_v4.csv',sep=', ',header=None,engine='python')
    df.columns = ['folder','fname','point','coordinates','commas']
    print('Annotations loading complete')
    df = df.iloc[:,0:4]
    return df

def load_point_annotation(point):
    df = load_annotations()
    new_df = df.loc[df['point']==str(point)+' ']
    print('Point ',point,' dataset size: ',new_df.shape)
    return new_df
# point = 'a1'
# df = load_point_annotation(point)

def crop_image(img):
    y,x = int(img.shape[0]/2),int(img.shape[1]/2)
    img = img[y:, x:]
    return img

def load_all_images(df):
    path_i = '/content/drive/My Drive/Colab/M2_vision/materialEnviadoVAMaster/dataset1/grafos_REY_roi_manualSelection1/'
    imgs,labels = [],[]
    for name in df['fname']:
        # Read image
        open_name = path_i+'grafo_'+name[:-1]+'.png'
        img = cv2.imread(open_name)
        if img is not None:
                img = crop_image(img)
                # Read label
                label = df['coordinates'].loc[df['fname']==name].values[0]
                imgs.append(img)
                labels.append(label)
                img,label=None,None

    print('Number of images added: ',len(imgs))
    print('Number of labels added: ',len(labels))

    return imgs,labels

def redim_labels(labels,point):
    n_coords = 1
    if point=='a1': n_coords = 2
    y = np.zeros((len(labels),n_coords)) # 2 coordinates for a1
    for i,cc in enumerate(labels):
        aux = cc.strip(',').strip('(').strip(')').strip(') ').strip(') ,').split(',')
        y[i] = [float(a) for a in aux]
    print('Shape of labels dataset: ',y.shape)
    return y

# Y = redim_labels(labels,point)

# add white pixels to fit all images into the same size
def adjust_dims(img,b,r):
    size = img.shape[:2]
    color = [0,0,0]
    bottom = b-size[0]
    right = r-size[1]
    out_img = cv2.copyMakeBorder(img, 0,bottom,0,right,cv2.BORDER_CONSTANT,value=color)
    return out_img

# reduce images size
from skimage.transform import rescale, resize
def reduce_imgs(img, reduct):
    img = resize(img, (img.shape[0]*reduct, img.shape[1]*reduct), anti_aliasing=False)
    return img

# dilate images
def dilate_imgs(img):
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(5,5))
    img = cv2.dilate(img, kernel)
    return img

# Plot annotations
def plot_annotation(X,y,idx):
    plt.figure()
    plt.imshow(X[idx], cmap='gray', interpolation='none', origin='upper')#, extent=[0,img_w,0,img_h])
    a = y[idx]
    plt.gca().add_patch(plt.Circle((a[0], a[1]), 10, color='r'))
    plt.show()

# Split training and testing set
def split_train_val_test(X,y,n):
    i_val = int((1-2*n)*X.shape[0])
    i_test = int((1-n)*X.shape[0])

    X_train = X[:i_val]
    X_val = X[i_val:i_test]
    X_test = X[i_test:]
    y_train = y[:i_val]
    y_val = y[i_val:i_test]
    y_test = y[i_test:]

    print('Size of training set:  ',X_train.shape[0])
    print('Size of validation set:  ',X_val.shape[0])
    print('Size of testing set:  ',X_test.shape[0])
    return X_train,X_val,X_test,y_train,y_val,y_test
