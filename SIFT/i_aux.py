import sys
import numpy as np
import numpy.linalg as LA

def gaussian_func(sigma): 
  size = 2*np.ceil(3*sigma)+1 
  x, y = np.mgrid[-size//2 + 1:size//2 + 1, -size//2 + 1:size//2 + 1] 
  g = np.exp(-((x**2 + y**2)/(2.0*sigma**2))) / (2*np.pi*sigma**2)
  return g/g.sum()

def cart_to_polar_grad(dx, dy): 
  m = np.sqrt(dx**2 + dy**2) 
  theta = (np.arctan2(dy, dx)+np.pi) * 180/np.pi 
  return m, theta 

def gradient(L, x, y): 
  dy = L[min(L.shape[0]-1, y+1),x]-L[max(0, y-1),x] 
  dx = L[y,min(L.shape[1]-1, x+1)]-L[y,max(0, x-1)] 
  return cart_to_polar_grad(dx, dy)

def quantize_orientation(theta, num_bins): 
  bin_width = 360//num_bins 
  return int(np.floor(theta)//bin_width)

def fit_parabola(hist, binno, bin_width): 
  centerval = binno*bin_width + bin_width/2. 
  if binno == len(hist)-1: rightval = 360 + bin_width/2. 
  else: rightval = (binno+1)*bin_width + bin_width/2. 
  if binno == 0: leftval = -bin_width/2. 
  else: leftval = (binno-1)*bin_width + bin_width/2. 
  A = np.array([ 
    [centerval**2, centerval, 1], 
    [rightval**2, rightval, 1], 
    [leftval**2, leftval, 1]]) 
  b = np.array([ 
    hist[binno], 
    hist[(binno+1)%len(hist)], 
    hist[(binno-1)%len(hist)]]) 
  x = LA.lstsq(A, b, rcond=None)[0] 
  if x[0] == 0: x[0] = 1e-6 
  return -x[1]/(2*x[0])

def get_patch_gradient(p): 
  r1 = np.zeros_like(p) 
  r1[-1] = p[-1] 
  r1[:-1] = p[1:] 
  r2 = np.zeros_like(p) 
  r2[0] = p[0] 
  r2[1:] = p[:-1] 
  dy = r1-r2 
  r1[:,-1] = p[:,-1] 
  r1[:,:-1] = p[:,1:] 
  r2[:,0] = p[:,0] 
  r2[:,1:] = p[:,:-1] 
  dx = r1-r2 
  return dx, dy

def get_histogram_for_subregion(m, theta, num_bin, reference_angle, bin_width, subregion_w): 
  hist = np.zeros(num_bin, dtype=np.float32) 
  c = subregion_w/2 - .5
  for i,(mag, angle) in enumerate(zip(m, theta)):
    angle = (angle-reference_angle) % 360        
    binno = quantize_orientation(angle, num_bin)        
    vote = mag      
   
    hist_interp_weight = 1 - abs(angle - (binno*bin_width + bin_width/2))/(bin_width/2)        
    vote *= max(hist_interp_weight, 1e-6)         
    gy, gx = np.unravel_index(i, (subregion_w, subregion_w))        
    x_interp_weight = max(1 - abs(gx - c)/c, 1e-6)            
    y_interp_weight = max(1 - abs(gy - c)/c, 1e-6)        
    vote *= x_interp_weight * y_interp_weight         
    hist[binno] += vote
  hist /= max(1e-6, LA.norm(hist)) 
  hist[hist>0.2] = 0.2 
  hist /= max(1e-6, LA.norm(hist))
  return hist