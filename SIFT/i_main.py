#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created by
@author: irene perez martinez
"""

from c_sift import SIFT
from p_featurematcher import fm_homography
from p_aux import plot_kp

import argparse
import cv2
import pickle
import os

import matplotlib.pyplot as plt


if __name__ == '__main__':

	# Load Model
	basepath = './DibujosMuestra/dataset1_reyDistorsiones/'
	savepath = './dataset1_results/dataset1_'
	file = 'Modelo.png'
	img1 = cv2.imread(basepath+file)
	gray1 = cv2.cvtColor(img1,cv2.COLOR_RGB2GRAY)

	sift_detector = SIFT(gray1)
	kp1, des1 = sift_detector.get_features()

	# print('Features detected: ',feats)
	plot_kp(img1,kp1)

	# Implement Matching System
	matcher = cv2.BFMatcher(cv2.NORM_L2, crossCheck=False)

	# Open images
	descriptors = []
	for file in os.listdir(basepath):

		# read image
		print('Reading image ',file)
		img2 = cv2.imread(basepath+file)
		gray2 = cv2.cvtColor(img2,cv2.COLOR_RGB2GRAY)

		# detect keypoints and compute descriptors
		sift_detector = SIFT(img2)
		# kp2, des2 = sift_detector.get_features()
		_ = sift_detector.get_features()
		kp2 = sift_detector.kp_pyr
		
		# plot keypoints
		plot_kp(img2,kp2)

		# Feature Matching
		fm_homography(img1,img2,kp1,kp2,des1,des2,4, matcher, 'sift_custom', file, savepath)
		