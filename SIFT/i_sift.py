#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created by
@author: irene
"""
import sys
import numpy as np

from scipy.ndimage.filters import convolve
from skimage.color import rgb2gray
import numpy.linalg as LA
from scipy.ndimage import gaussian_filter

from i_aux import gaussian_func
from i_aux import cart_to_polar_grad, gradient, quantize_orientation, fit_parabola
from i_aux import get_patch_gradient, get_histogram_for_subregion

# Detección de extremos en el espacio-escala
def generate_octave(img, s, sigma): 
  octave = [img] 
  k = 2**(1/s)
  for _ in range(s+2): 
    # apply gaussian filter (from scipy) iteratively
    new_img = gaussian_filter(octave[-1],k*sigma) 
    octave.append(new_img) 
  return octave

def generate_LoG_pyramid(im, num_octave, s, sigma): 
  pyr = [] 
  for _ in range(num_octave): 
    octave = generate_octave(im, s, sigma) 
    pyr.append(octave) 
    im = octave[-3][::2, ::2] 
  return pyr

def generate_DoG_octave(LoG_octave): 
  octave = [] 
  for i in range(1, len(LoG_octave)): 
    # compute difference of laplacian-of-gaussians 
    octave.append(LoG_octave[i]-LoG_octave[i-1]) 
  return np.concatenate([o[:,:,np.newaxis] for o in octave], axis=2)

def generate_DoG_pyramid(LoG_pyramid): 
  pyr = [] 
  for LoG_octave in LoG_pyramid: 
    pyr.append(generate_DoG_octave(LoG_octave)) 
  return pyr

def get_candidate_keypoints(DoG, w=16):
  candidates = []
  DoG[:,:,0],DoG[:,:,-1] = 0,0
  # compute over three different scales
  # 3x3 neighbors
  for i in range((w//2)+1, DoG.shape[0]-(w//2)-1):
    for j in range(w//2+1, DoG.shape[1]-w//2-1): 
      for k in range(1, DoG.shape[2]-1): 
        patch = DoG[i-1:i+2, j-1:j+2, k-1:k+2] 
        # detect if the point is a maximum or minimum extrema
        if np.argmax(patch) == 13 or np.argmin(patch) == 13: 
          candidates.append([i, j, k]) 
  return candidates

# To refine these candidates:
def localize_keypoint(D, x, y, s): 
  # using Taylor series for each DoG
  # calculate hessian and jacobian matrix of each DoG
  dx = (D[y,x+1,s]-D[y,x-1,s])/2. 
  dy = (D[y+1,x,s]-D[y-1,x,s])/2. 
  ds = (D[y,x,s+1]-D[y,x,s-1])/2. 
  dxx = D[y,x+1,s]-2*D[y,x,s]+D[y,x-1,s] 
  dxy = ((D[y+1,x+1,s]-D[y+1,x-1,s]) - (D[y-1,x+1,s]-D[y-1,x-1,s]))/4. 
  dxs = ((D[y,x+1,s+1]-D[y,x-1,s+1]) - (D[y,x+1,s-1]-D[y,x-1,s-1]))/4. 
  dyy = D[y+1,x,s]-2*D[y,x,s]+D[y-1,x,s] 
  dys = ((D[y+1,x,s+1]-D[y-1,x,s+1]) - (D[y+1,x,s-1]-D[y-1,x,s-1]))/4. 
  dss = D[y,x,s+1]-2*D[y,x,s]+D[y,x,s-1] 
  J = np.array([dx, dy, ds]) 
  HD = np.array([ [dxx, dxy, dxs], [dxy, dyy, dys], [dxs, dys, dss]]) 
  # calculating the extrema of DoG brings the next equation:
  offset = -LA.inv(HD).dot(J)
  return offset, J, HD[:2,:2], x, y, s

# refine the candidates
def find_keypoints_for_DoG_octave(D, R_th, t_c, w): 
  candidates = get_candidate_keypoints(D, w)
  keypoints = [] 
  for _, cand in enumerate(candidates): 
    y, x, s = cand[0], cand[1], cand[2] 
    offset, J, H, x, y, s = localize_keypoint(D, x, y, s) 

    # eliminate pixels with contrast below threshold, using Taylor series of DoG with the offset value
    contrast = D[y,x,s] + .5*J.dot(offset) 
    if abs(contrast) < t_c: continue 

    # find eigenvalues of the Hessian to detect if the pixels is a corner (Harris corner detection)
    l, v = LA.eig(H) 
    r = l[1]/l[0] 
    # compute parameter R = l_1 l_2 - k(l_1 + l_2)^2
    R = (r+1)**2 / r 
    # if R is positive and large it is a corner 
    if R > R_th: continue 

    # finally, assign as keypoints with subpixel
    kp = np.array([x, y, s]) + offset
    keypoints.append(kp)
  return np.array(keypoints)

# compute all keypoints
def get_keypoints(DoG_pyr, R_th, t_c, w): 
  keypoints = [] 
  for DoG in DoG_pyr: 
    keypoints.append(find_keypoints_for_DoG_octave(DoG, R_th, t_c, w)) 
  return keypoints

# assign orientation to each keypoint
def assign_orientation(kps, octave, num_bins=36): 
  new_kps = [] 
  bin_width = 360//num_bins 
  for kp in kps: 
    cx, cy, s = int(kp[0]), int(kp[1]), int(kp[2]) 
    s = np.clip(s, 0, octave.shape[2]-1) 
    sigma = kp[2]*1.5 
    w = int(2*np.ceil(sigma)+1) 
    kernel = gaussian_func(sigma) 
    L = octave[...,s]
    hist = np.zeros(num_bins, dtype=np.float32) 
    for oy in range(-w, w+1): 
      for ox in range(-w, w+1): 
        x, y = cx+ox, cy+oy 
        if x < 0 or x > octave.shape[1]-1: continue 
        elif y < 0 or y > octave.shape[0]-1: continue 
        # calculate gradient for each interest pixel
        m, theta = gradient(L, x, y) 
        weight = kernel[oy+w, ox+w] * m 
        # assign theta in a bin of the histogram
        bin = quantize_orientation(theta, num_bins) 
        hist[bin] += weight 
    # assign orientation of the maximal histogram bin to each keypoint
    max_bin = np.argmax(hist) 
    new_kps.append([kp[0], kp[1], kp[2], fit_parabola(hist, max_bin, bin_width)]) 
    # max_val = np.max(hist) 
    # for binno, val in enumerate(hist): 
    #   if binno == max_bin: continue 
    #   if .8 * max_val <= val: 
    #     new_kps.append([kp[0], kp[1], kp[2], fit_parabola(hist, binno, bin_width)])
  return np.array(new_kps)

def get_local_descriptors(kps, octave, w=16, num_subregion=4, num_bin=8): 
  descs = [] 
  bin_width = 360//num_bin
  for kp in kps: 
    cx, cy, s = int(kp[0]), int(kp[1]), int(kp[2]) 
    s = np.clip(s, 0, octave.shape[2]-1) 
    kernel = gaussian_func(w/6) # gaussian_filter multiplies sigma by 3 
    L = octave[...,s] 
    t, l = max(0, cy-w//2), max(0, cx-w//2) 
    b, r = min(L.shape[0], cy+w//2+1), min(L.shape[1], cx+w//2+1) 
    # select a patch of 16x16 centered in the current pixel keypoint
    patch = L[t:b, l:r] 
    dx, dy = get_patch_gradient(patch) 
    if dx.shape[0] < w+1: 
      if t == 0: kernel = kernel[kernel.shape[0]-dx.shape[0]:] 
      else: kernel = kernel[:dx.shape[0]] 
    if dx.shape[1] < w+1: 
      if l == 0: kernel = kernel[kernel.shape[1]-dx.shape[1]:] 
      else: kernel = kernel[:dx.shape[1]] 
    if dy.shape[0] < w+1: 
      if t == 0: kernel = kernel[kernel.shape[0]-dy.shape[0]:] 
      else: kernel = kernel[:dy.shape[0]] 
    if dy.shape[1] < w+1: 
      if l == 0: kernel = kernel[kernel.shape[1]-dy.shape[1]:] 
      else: kernel = kernel[:dy.shape[1]] 
    m, theta = cart_to_polar_grad(dx, dy) 
    dx, dy = dx*kernel, dy*kernel 
    # divide into 16 subregions of 4x4 pixels
    subregion_w = w//num_subregion 
    featvec = np.zeros(num_bin * num_subregion**2, dtype=np.float32) 
    for i in range(0, subregion_w): 
      for j in range(0, subregion_w): 
        t, l = i*subregion_w, j*subregion_w 
        b, r = min(L.shape[0], (i+1)*subregion_w), min(L.shape[1], (j+1)*subregion_w) 
        # compute the histogram of gradients for each subregion
        hist = get_histogram_for_subregion(m[t:b, l:r].ravel(), theta[t:b, l:r].ravel(), num_bin, kp[3], bin_width, subregion_w) 
        # concatenate all histograms in a 128-feature vector
        featvec[i*subregion_w*num_bin + j*num_bin:i*subregion_w*num_bin + (j+1)*num_bin] = hist.flatten() 
    # normalize the vector
    featvec /= max(1e-6, LA.norm(featvec))
    # threshold the vector        
    featvec[featvec>0.2] = 0.2
    # renormalize the vector        
    featvec /= max(1e-6, LA.norm(featvec))    
    descs.append(featvec)
  return np.array(descs)




class SIFT(object): 
  # number of octaves = 4, number of scale levels = 5, initial σ=1.6, k=2–√2 etc as optimal values.
  def __init__(self, im, s=5, num_octave=4, s0=1.6, sigma=np.sqrt(2), r_th=10, t_c=0.03, w=16): 
    self.im = gaussian_filter(rgb2gray(im),s0)
    self.s = s 
    self.sigma = sigma 
    self.num_octave = num_octave
    self.t_c = t_c 
    self.R_th = (r_th+1)**2 / r_th 
    self.w = w 
  def get_features(self): 
    LoG_pyr = generate_LoG_pyramid(self.im, self.num_octave, self.s, self.sigma) 
    DoG_pyr = generate_DoG_pyramid(LoG_pyr) 
    kp_pyr = get_keypoints(DoG_pyr, self.R_th, self.t_c, self.w) 
    feats = [] 
    for i, DoG_octave in enumerate(DoG_pyr): 
        kp_pyr[i] = assign_orientation(kp_pyr[i], DoG_octave) 
        feats.append(get_local_descriptors(kp_pyr[i], DoG_octave)) 
    return kp_pyr, feats













