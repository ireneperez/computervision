import pickle
import os
import cv2
import matplotlib.pyplot as plt
import numpy as np

from p_featurematcher import fm_homography 
from p_aux import find_d_m, detect_compute, plot_kp

# Load method
string_d, string_m = 'sift', 'flann'
descriptor, matcher = find_d_m(string_d,string_m)

# Load Model
basepath = './DibujosMuestra/dataset1_reyDistorsiones/'
savepath = './dataset1_results/dataset1_'
file = 'Modelo.png'
img1 = cv2.imread(basepath+file)
gray1 = cv2.cvtColor(img1,cv2.COLOR_RGB2GRAY)

kp1,des1 = detect_compute(string_d,descriptor,gray1)
plot_kp(img1,kp1)


# Open images
descriptors = []
for file in os.listdir(basepath):

    # read image
    print('Reading image ',file)
    img2 = cv2.imread(basepath+file)
    gray2 = cv2.cvtColor(img2,cv2.COLOR_RGB2GRAY)

    # detect keypoints and compute descriptors
    kp2,des2 = detect_compute(string_d,descriptor,gray2)
    
    # plot keypoints
    # plot_kp(img2,kp2)

    # Feature Matching
    fm_homography(img1,img2,kp1,kp2,des1,des2,4, matcher, string_d, file, savepath)